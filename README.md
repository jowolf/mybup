My bup helper scripts
=====================

JJW last updated 4/23/19

init.sh - initialize a new backup drive with a bup/ dir and a .bup/ repo inside it

bup.sh - backup this computer, meant to be run from the bup/ dir on the backup drive
newbup.sh - bups several dirs at once, groups backups by host not dir - swap w/above, soon

fup.sh - folder backup, meant to be run from the dir containing the .bup/ repo, can be a backup drive or home folder
Uses sensible defaults, adds a bname if present

web.sh - launches mini-web server on 8080, prints URL and PID for surf / kill respectively

fus.sh - launches fuse fs on /media/bup, handy for file dates/times & metadata - killall bup-fuse

rup.sh - remote bup - run on local server, pass <remote host>:<path to .bup dir>

make-softlinks.sh - install softlinks to relevant scripts, to be used in /media/<backup drive>/bup dir
 (the one containing the .bup dir, so it should be /media/<backup drive>/bup/.bup after init.sh is run)

exclude-from.txt - list of files and dirs to exclude when bupping - add dirs w/>1M files here as needed

NFG:
orup.sh - on remote bup - run on server to 'pull' backup from host - nfg, fails on ssh connection


Installation 
============

Locally, for bup to removable media / 2nd drive:

- Plug in USB backup drive (or use other / existing 2nd drive)
- cd /media/<backup drive>
- git clone <this repo>
- mybup/init.sh  (Should also run make-softlinks.sh)

Locally, for remote bup (rup.sh):

From home/<user>:
- sudo apt install git bup
- git clone <this repo>, typically https://gitlab.com/jowolf/mybup.git
- bup init
- install maya ssh key
- connect once via ssh to establish known host

Best results with ssh key and entry in .ssh/config, bup's ssh internals won't always prompt for pw.

Here is a sample .ssh/config entry:

Host 192.168.1.25
  User root
  IdentityFile /home/joe/.ssh/id_rsa_maya


Local Operation
===============

From removable / 2nd drive:
- cd /media/<backup drive>/bup
- ./newbup.sh


Remote Operation
================

- .bup dir (for index) should be in cur dir
- exclude-from.txt should be in same dir with .rup.sh

Locally, to remote (requires local bup init):
- ./mybup/rup.sh <remote host>:<path to remote .bup dir>

Locally, uses existing .bup dir on local backup drive for index, but saves to remote:

- cd /media/joe/<backup drive>/bup/
- .rup.sh <remote host>:<path to remote .bup dir>

Locally, uses home ~/.bup dir (formed by bup init) for index, and code on backup drive, but saves to remote:

- /media/joe/<backup drive>/mybup/rup.sh <remote host>:<path to remote .bup dir>


TODO
====

d - assemble rup.sh from humboldt history to run locally
- include remote fsck at end?
d - requires: local bup installed, preferably close to server version
d - .ssh/config entry for server w/identity


7/21/18 streamline save to hostname/<date>/dirs

deleted junky comments:

# nfg, doesn't pass throu to sudo:
#env BUP_DIR=`pwd`/.bup
#echo

# nfg, doesn't work either:
#sudo -E env
#sudo --preserve-env env
#exit

5/27/18 enhance for Mac folders, added fus, web, add mult folders to index & save for better naming, etc

2/19/18 excluded /var/lib/lxcfs, added make-softlinks.sh

11/25/17 added exclude-from, Mac folders

10/22/17 JJW initial release - My bup helper scripts
