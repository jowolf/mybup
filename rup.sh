# rup.sh - Back up local host to remote host with bup repo
#
# - can either be run from home or backup drive
# - requires bup locally
# - must be run from mybup repo
# - can be run from repo on backup drive (only used for repo, not backup), or ~/mybup in home dir
# - runs index locally, save remotely
# - needs key for ssh - see README/md
#
# Usage:
#
# ./rup.sh <remote host:path>
#
#
# Example (run from home/joe):
#
# /media/joe/Elements1/mybup/rup.sh 192.168.1.25:/media/joe/Seagate-5T/bup/.bup
#
#
# Example (run from /media/joe/Elements1/bup, uses .bup dir for index on backup drive):
#
# ./rup.sh 192.168.1.25:/media/joe/Seagate-5T/bup/.bup
#
#
# still TBD: add redundancy


bupdir=`pwd`/.bup
hostname=`hostname -s`
exclude=`dirname $0`/exclude-from.txt

echo Testing connection to $1
echo ls | sftp -b - $1


if echo $OSTYPE | grep darwin; then {   # Could also use uname here

  dirs='/etc /home /opt /root /var /usr/local /Users /private'

} elif echo $OSTYPE | grep linux; then {

  dirs='/etc /home /opt /root /var /usr/local'

} else {
  echo Unknown platform - $OSTYPE
  exit
} fi


echo
echo Backing up $hostname $dirs with bup...
echo Locally to $bupdir then remotely to $1
echo Excluding $exclude
echo

#set -x
sudo bup -d $bupdir index $dirs --exclude-from $exclude
sudo -E bup -d $bupdir save -r $1 -n $hostname $dirs
#set +x

echo TODO: add redundancy via ssh remote cmd
exit


Todo: remote fsck via ssh

echo 'bup index & save done, adding redundancy'

if sudo bup -d $bupdir fsck --par2-ok; then {
  echo "par2 is ok, doing bup fsck"
  sudo bup -d $bupdir fsck -gv --quick
} else {
  echo "par2 is NOT INSTALLED - no redundancy will be added"
} fi

echo bup done.
